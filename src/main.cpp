#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266WebServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <LittleFS.h>
#include <SoftwareSerial.h>
#include <WiFiClient.h>

// General Constants
const bool CEREALOS_DEBUG_MODE = false;

const int INGREDIENT_COUNT = 3;
const int RECIPE_COUNT = 4;

const byte DEFAULT_DIRECTION = false;

const int STATUS_LED_PIN = LED_BUILTIN; // D4
const String SOFTWARE_FINGERPRINT = "cerealos_0.9";
const String AP_SSID = "cerealOS_recovery";

// Watchdog values
const long MANUAL_DISPENSE_MAX_AMOUNT = 100000000;

//// Motors
// General
const unsigned int MOTOR_MAX = 8;
const unsigned int MOTOR_COUNT = 7;

// Pins
const unsigned int MOTORS_DIRECTION = D8;
const unsigned int MOTORS_ENABLE = D7;

// STEP shift register
const unsigned int STEP_SHIFT_DATA = D1;
const unsigned int STEP_SHIFT_CLOCK = D2;
const unsigned int STEP_SHIFT_LATCH = D3;

// States
const unsigned int LIVE_STATE = 0;
const unsigned int RECOVERY_STATE = 1;

// Recovery client modes
const String CONFIG_FLASH_MODE = "CONFIG_FLASH_MODE";
const String RECIPE_FLASH_MODE = "RECIPE_FLASH_MODE";

const String CONFIG_LOAD_MODE = "CONFIG_LOAD_MODE";
const String RECIPE_LOAD_MODE = "RECIPE_LOAD_MODE";

const IPAddress RECOVERY_IP(192, 168, 10, 1);
const int RECOVERY_PORT = 80;

//// Touchscreen zach
// Touchscreen serial
const int NEXTION_RX = D5;
const int NEXTION_TX = D6;
SoftwareSerial nextion(NEXTION_RX, NEXTION_TX);

// Touchscreen protocol
const int commandBytes = 2;

//// Configuration
// Pathes
const String CONFIG_FILE_PATH = "config.json";
const String RECIPE_FILE_PATH = "recipes.json";
const String VERSION_FILE_PATH = "version";

// Keys
const String CEREAL_SIZES_KEY = "cereal_sizes";
const String MOTOR_STEP_DELAY_MICROS_KEY = "motor_step_delay_micros";
const String MANUAL_DISPENSE_STEP_COUNT_KEY = "manual_dispense_step_count";

// Postloaded values
int recipePercentages[RECIPE_COUNT][INGREDIENT_COUNT];
int recipeModulator[RECIPE_COUNT][INGREDIENT_COUNT];

long cerealSizes[3] = {100, 1000, 10000};
int motorStepDelayMicros = 800;
unsigned int manualDispenseStepCount = 100;

//// Update server
ESP8266WebServer recServer(80);

//// Runtime
// Postload
bool mounted = false;
bool configLoaded = false;

// Ingredients
int dispenseIngredient = -1;
int dispensedAmount = 0;

// State
int state = 0;
bool recoverySetup = false;

//// Runtime settings

/* 
    Presets
    0 -> Small (UI Default)
    1 -> Medium
    2 -> Big
*/
unsigned int cerealSizePreset = 0;

// -- Math
void bakeModulators(int sizeIndex)
{
    for (unsigned int recipe = 0; recipe < RECIPE_COUNT; recipe++)
        for (unsigned int ingredient = 0; ingredient < INGREDIENT_COUNT; ingredient++)
        {
            double percentageValue = ((cerealSizes[sizeIndex] / 100) * recipePercentages[recipe][ingredient]);
            double modulator = 0;
            if (percentageValue != 0)
                modulator = round(cerealSizes[sizeIndex] / percentageValue);
            recipeModulator[recipe][ingredient] = modulator;
        }
}

// ----- Motors
void enableMotors()
{
    digitalWrite(MOTORS_ENABLE, LOW);
    delay(motorStepDelayMicros);
}

void disableMotors()
{
    digitalWrite(MOTORS_ENABLE, HIGH);
    delay(motorStepDelayMicros);
}

void step(int index)
{
    if (!CEREALOS_DEBUG_MODE)
    {
        // Build data
        byte data = 0;
        if (index < 8 && index >= 0)
            bitSet(data, index);

        // Write to shift register
        digitalWrite(STEP_SHIFT_LATCH, LOW);
        shiftOut(STEP_SHIFT_DATA, STEP_SHIFT_CLOCK, MSBFIRST, data);
        digitalWrite(STEP_SHIFT_LATCH, HIGH);
        delayMicroseconds(motorStepDelayMicros);

        // Clear shift register
        data = 0;
        digitalWrite(STEP_SHIFT_LATCH, LOW);
        shiftOut(STEP_SHIFT_DATA, STEP_SHIFT_CLOCK, MSBFIRST, data);
        digitalWrite(STEP_SHIFT_LATCH, HIGH);
        delayMicroseconds(motorStepDelayMicros);
    }
    else
    {
        digitalWrite(LED_BUILTIN, LOW);
        delay(10);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(10);
    }
}

// -- Execution layer
void doRecipe(int index, int size)
{
    bakeModulators(size);

    enableMotors();

    // Iterate all steps
    for (unsigned long steps = 0; steps < cerealSizes[size]; steps++)
    {
        // Iterate ingredients
        for (unsigned int ingredient = 0; ingredient < INGREDIENT_COUNT; ingredient++)
        {
            // Check if ingredient has to do a step
            int modulator = recipeModulator[index][ingredient];
            if (modulator != 0 && steps % modulator == 0)
            {
                step(ingredient);
            }
        }
    }
    disableMotors();
}

void loopIngredient()
{
    // Dispense ingredient
    if (dispenseIngredient != -1)
    {
        step(dispenseIngredient);
        dispensedAmount++;
    }

    // Check if overflow is reached
    if (dispensedAmount > MANUAL_DISPENSE_MAX_AMOUNT)
    {
        dispenseIngredient = -1;
        disableMotors();
    }
}

// -- Ui layer
bool isCommandAvailable()
{
    return nextion.available() >= commandBytes;
}

byte *readCommand()
{
    byte buffer[commandBytes];
    nextion.readBytes(buffer, sizeof(buffer));
    return buffer;
}

void executeCommand(int command, int parameter)
{
    if (command == 0)
    {
        // STATE CHANGED
        if (CEREALOS_DEBUG_MODE)
            Serial.print("NEXTION STATE IS NOW " + String(parameter));
    }
    else if (command == 1)
    {
        // EXECUTE RECIPE
        doRecipe(parameter, cerealSizePreset);
        if (CEREALOS_DEBUG_MODE)
            Serial.print("EXECUTE RECIPE WITH INDEX " + String(parameter));
    }
    else if (command == 2)
    {
        // START DISPENSING INGREDIENT
        dispensedAmount = 0;
        dispenseIngredient = parameter;
        if (CEREALOS_DEBUG_MODE)
            Serial.print("START DISPENSING INGREDIENT WITH INDEX " + String(parameter));
        enableMotors();
    }
    else if (command == 3)
    {
        // STOP DISPENSING INGREDIENT
        dispensedAmount = 0;
        dispenseIngredient = -1;
        if (CEREALOS_DEBUG_MODE)
            Serial.print("STOP DISPENSING INGREDIENT WITH INDEX " + String(parameter));
        disableMotors();
    }
    else if (command == 4)
    {
        // CHANGE OPERATION MODE
        state = parameter;
    }
}

// -- Files
String loadFile(String file)
{
    // Open file
    File source = LittleFS.open(file, "r");

    // Read file
    String str = "";
    while (source.available())
        str += (char)source.read();

    // Close file
    source.close();

    return str;
}

bool flashJson(String file, String data)
{
    bool written = false;
    while (!written)
    {
        // Write file
        File dest = LittleFS.open(file, "w");
        for (const char c : data)
        {
            dest.write(c);
            Serial.print(c);
        }
        Serial.println();
        dest.close();

        // Read file and compare to source content
        if (loadFile(file) == data)
        {
            written = true;
        }
        else
        {
            Serial.println("Write failed! Redo...");
            delay(100);
        }
    }
    return true;
}

// -- Recovery
void recoveryHandleRequest()
{
    if (recServer.hasArg("operation"))
    {
        String operation = recServer.arg("operation");
        String plain = recServer.arg("plain");

        if (operation == CONFIG_FLASH_MODE && recServer.hasArg("plain"))
        {
            if (flashJson(CONFIG_FILE_PATH, plain))
                recServer.send(200);
            else
                recServer.send(400);
        }
        else if (operation == CONFIG_LOAD_MODE)
        {
            recServer.send(200, "application/json", loadFile(CONFIG_FILE_PATH));
            Serial.println("Config file sent! " + loadFile(CONFIG_FILE_PATH));
        }
        else if (operation == RECIPE_FLASH_MODE && recServer.hasArg("plain"))
        {
            if (flashJson(RECIPE_FILE_PATH, plain))
                recServer.send(200);
            else
                recServer.send(400);
        }
        else if (operation == RECIPE_LOAD_MODE)
        {
            recServer.send(200, "application/json", loadFile(RECIPE_FILE_PATH));
            Serial.println("Recipe file sent! " + loadFile(CONFIG_FILE_PATH));
        }
        else
            recServer.send(400);
    }
    else
        recServer.send(400);
}

void setup()
{

    // !! Temporarly removed. Incompatibility with CerealizerCB (Version 1)
    // pinMode(D0, INPUT);
    // delay(2000);
    // if (digitalRead(D0) == HIGH)
    //     state = 1;

    //// General
    // Debug serial
    Serial.begin(9600);
    Serial.println();
    Serial.println("Welcome! Starting up...");
    Serial.print("Software fingerprint: ");
    Serial.println(SOFTWARE_FINGERPRINT);

    // Debug LED
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, HIGH);

    // Mount filesystem
    Serial.println("Mounting filesystem...");
    mounted = LittleFS.begin();
    if (!mounted)
    {
        Serial.println("First mount failed! Formatting...");
        LittleFS.format();
        mounted = LittleFS.begin();
    }
    if (mounted)
        Serial.println("Filesystem mounted!");
    else
        Serial.println("Filesystem mounting failed!");

    // Format if not version compliant
    File versionr = LittleFS.open(VERSION_FILE_PATH, "r");
    String versionFileString = versionr.readString();
    versionr.close();
    if (versionFileString != SOFTWARE_FINGERPRINT)
    {
        Serial.println("Filesystem intgrity violation! Reformat and reboot...");
        LittleFS.format();
        File versionw = LittleFS.open(VERSION_FILE_PATH, "w");
        versionw.print(SOFTWARE_FINGERPRINT);
        versionw.close();
        Serial.println("Integrity recovered! Restart initiated!");
        ESP.restart();
    }
    else
    {
        Serial.println("Filesystem integrity check ok!");
    }

    //// Postloading
    configLoaded = false;
    if (mounted)
    {
        Serial.println("Postloading started...");

        //// Load config
        Serial.println("Load config...");
        StaticJsonDocument<1024> configJson;
        File cfile = LittleFS.open(CONFIG_FILE_PATH, "r");
        DeserializationError cJsonErr = deserializeJson(configJson, cfile);
        cfile.close();
        if (cJsonErr == DeserializationError::Ok)
        {
            configLoaded = true;
            Serial.println("Config loaded!");
        }
        else
        {
            configLoaded = false;
            Serial.print("Config loading failed! ");
            Serial.println(cJsonErr.c_str());
        }
        // Extract information
        motorStepDelayMicros = configJson[MOTOR_STEP_DELAY_MICROS_KEY];
        copyArray(configJson[CEREAL_SIZES_KEY], cerealSizes);
        manualDispenseStepCount = configJson[MANUAL_DISPENSE_STEP_COUNT_KEY];

        //// Load recipes
        Serial.println("Loading recipes...");
        StaticJsonDocument<1024> recipeJson;
        File rfile = LittleFS.open(RECIPE_FILE_PATH, "r");
        DeserializationError rJsonErr = deserializeJson(recipeJson, rfile);
        rfile.close();
        if (rJsonErr == DeserializationError::Ok)
        {
            configLoaded &= true;
            Serial.println("Recipes loaded!");
        }
        else
        {
            configLoaded = false;
            Serial.print("Recipe loading failed! ");
            Serial.println(rJsonErr.c_str());
        }

        // Extract recipes
        copyArray(recipeJson.as<JsonArray>(), recipePercentages);
        // for (int recipe = 0; recipe < RECIPE_COUNT; recipe++)
        //     for (int ingredient = 0; ingredient < INGREDIENT_COUNT; ingredient++)
        //         recipePercentages[recipe][ingredient] = recipeJson[recipe][ingredient].as<int>();
    }

    // Output if config or recipe loading failed
    if (configLoaded)
    {
        Serial.println("Config entirely loaded!");
    }
    else
    {
        Serial.println("Config not loaded! At least not entirely: Read log above!");
        for (int i = 0; i < 3; i++)
        {
            digitalWrite(STATUS_LED_PIN, LOW);
            delay(100);
            digitalWrite(STATUS_LED_PIN, HIGH);
            delay(100);
        }
    }

    if (!CEREALOS_DEBUG_MODE)
    {
        pinMode(MOTORS_ENABLE, OUTPUT);
        pinMode(MOTORS_DIRECTION, OUTPUT);

        // Step shift register
        pinMode(STEP_SHIFT_DATA, OUTPUT);
        pinMode(STEP_SHIFT_CLOCK, OUTPUT);
        pinMode(STEP_SHIFT_LATCH, OUTPUT);

        digitalWrite(MOTORS_DIRECTION, DEFAULT_DIRECTION);

        disableMotors();
    }

    //// Recovery
    recServer.on("/", recoveryHandleRequest);
    recServer.begin();

    // Touchscreen
    nextion.begin(9600);
}

void loopRecovery()
{
    if (!recoverySetup)
    {
        recoverySetup = true;

        // Start WiFi AP
        if (!WiFi.softAPConfig(IPAddress(192, 168, 10, 1), IPAddress(192, 168, 10, 1), IPAddress(255, 255, 255, 0)))
            Serial.println("Recovery setup failed!");
        WiFi.softAP(AP_SSID);

        Serial.println("Recovery access point online! IP: " + WiFi.softAPIP().toString());
    }
    recServer.handleClient();
}

void loopLive()
{
    if (isCommandAvailable())
    {
        byte *rawCommand = readCommand();

        int command = static_cast<int>(rawCommand[0]);
        int parameter = static_cast<int>(rawCommand[1]);

        if (CEREALOS_DEBUG_MODE)
        {
            Serial.println("x ------------ x");
            Serial.println("| Command: " + String(command) + "   |");
            Serial.println("| Parameter: " + String(parameter) + " |");
            Serial.print("| ");
        }

        executeCommand(command, parameter);

        Serial.println(" |");
    }
    loopIngredient();
}

void loop()
{
    if (state == LIVE_STATE)
    {
        loopLive();
    }
    else if (state == RECOVERY_STATE)
    {
        loopRecovery();
    }

    /*enableMotors();
    Serial.println("Motor an");
    for (int i = 0; i < 1000; i++)
    {
        ESP.wdtFeed();
        step(0);
        step(2);
    }
    disableMotors();
    Serial.println("Motor aus");
    delay(1000);
    */
}